# -*- coding: utf-8 -*-
#
# Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 
# - Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# 
# - Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
# 
# - Neither the name of the hhsprings nor the names of its contributors
#   may be used to endorse or promote products derived from this software
#   without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# =========================================================================
#
# character-sets-1.csv is from:
#     http://www.iana.org/assignments/character-sets/character-sets.xhtml
#
import os

_MYDIR = os.path.dirname(os.path.abspath(__file__))
_PICKLED = os.path.join(_MYDIR, "iana_character_sets.pickle")

def _mangle_key(s):
    return s.replace("-", "_").lower()


def _build_dict():
    import codecs
    import csv
    reader = csv.DictReader(
        open(os.path.join(_MYDIR, "character-sets-1.csv"), "rb"))
    result = {}
    for row in reader:
        trynames = [row["Name"]]
        if row["Preferred MIME Name"]:
            trynames.append(row["Preferred MIME Name"])
        if row["Aliases"]:
            trynames.extend(row["Aliases"].split("\n"))

        pycodecnames = set()
        nf = []
        for nm in trynames:
            try:
                pyc = codecs.lookup(nm)
                result[_mangle_key(nm)] = (
                    pyc.name, row["Preferred MIME Name"])
                pycodecnames.add(pyc)
            except LookupError:
                nf.append(nm)
        if pycodecnames and nf:
            pyc = list(pycodecnames)[0]
            for nm in nf:
                result[_mangle_key(nm)] = (
                    pyc.name, row["Preferred MIME Name"])
    return result


def _get_dict():
    """
    return dict, as:
        key: iena name (or alias)
        value: tuple of python codec name and preferred MIME name
    """
    import pickle
    if os.path.exists(_PICKLED):
        return pickle.load(open(_PICKLED, "rb"))
    result = _build_dict()
    pickle.dump(result, open(_PICKLED, "wb"))
    return result


_IANA_MAPPING = _get_dict()
del _build_dict
del _get_dict


def lookup(charset_name):
    """
    if exists, return tuple of python codec name and preferred MIME name,
    otherwise raise LookupError
    """
    key = _mangle_key(charset_name)
    if key in _IANA_MAPPING:
        return _IANA_MAPPING[key]
    for k in _IANA_MAPPING:
        if charset_name == _IANA_MAPPING[k][0]:
            return _IANA_MAPPING[k]
    raise LookupError("unsupported charset: %s" % charset_name)
