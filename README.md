pygmentize.cgi
==============
Welcome to pygmentize.cgi website.

Documentations are all in [http://hhsprings.pinoko.jp/pygmentize_cgi/cgi-bin/pygmentize_cgi.html](http://hhsprings.pinoko.jp/pygmentize_cgi/cgi-bin/pygmentize_cgi.html), so please see it.

[![support pygmentize.cgi with tiny amounts of money via PayPal](https://www.paypal.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=Z9CBS6G3MLFRN&currency_code=USD&lc=en_US)
