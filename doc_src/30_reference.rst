.. #
   # Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings>
   # All rights reserved.
   # 
   # Redistribution and use in source and binary forms, with or without
   # modification, are permitted provided that the following conditions
   # are met:
   # 
   # - Redistributions of source code must retain the above copyright
   #   notice, this list of conditions and the following disclaimer.
   # 
   # - Redistributions in binary form must reproduce the above copyright
   #   notice, this list of conditions and the following disclaimer in
   #   the documentation and/or other materials provided with the
   #   distribution.
   # 
   # - Neither the name of the hhsprings nor the names of its contributors
   #   may be used to endorse or promote products derived from this software
   #   without specific prior written permission.
   # 
   # THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   # "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
   # TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   # PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
   # CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   # EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   # PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   # PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   # LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   # NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   # SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   #

Reference
*********

Basics
------
The query parameters for pygmentize.cgi is structured with json:

.. code-block:: json

  {
    "command": "format",
    "params": {
    }
  }

``command`` is ``format`` or ``lexers``. ``params`` is diference between ``format``
and ``lexers``. See below in this document.

You can control the behaviour of CGI via some of HTTP layer headers,
for example (in Python):

.. code-block:: python

  >>> req = urllib2.Request(URL, data=json.dumps({
  ...     "command": "lexers",
  ...     "params": {"fields": ["name", "aliases", "filenames", "mimetypes"]},
  ... }))
  >>> # we need compress with gzip
  ...
  >>> req.add_header("Accept-Encoding", "gzip")

params for "command": "lexers"
------------------------------
The only ``params`` for ``"command": "lexers"`` is ``fields``.

Internally, this functionality is owed to calling like this:

.. code-block:: python

  import pygments
  from pygments.lexers import get_all_lexers
  for name, aliases, filenames, mimetypes in get_all_lexers():
    # ...

i.e, ``fields`` parameter just relate it directly.

You can choise fields from name, aliases, filenames, and mimetypes::

  >>> req = urllib2.Request(URL, data=json.dumps({
  ...     "command": "lexers",
  ...     "params": {"fields": ["name", "aliases", "filenames", "mimetypes"]},
  ... }))

In most cases, maybe you just want to build pulldown for selection of lexer,
if so, you just need ``name`` and ``aliases``.


params for "command": "format"
------------------------------
``params`` for ``"command": "format"`` is almost the same as
``pygments.formatters.html.HTMLFormatter`` (see details in `pygments source <https://bitbucket.org/birkenfeld/pygments-main/src>`_).

Next section is the document of pygments.formatters.html.HTMLFormatter,
not of pygmentize.cgi. But the difference is a little and tiny,
so, I will describe such differences later.


The document of pygments.formatters.html.HTMLFormatter
######################################################
Format tokens as HTML 4 ``<span>`` tags within a ``<pre>`` tag, wrapped
in a ``<div>`` tag. The ``<div>``'s CSS class can be set by the `cssclass`
option.

If the `linenos` option is set to ``"table"``, the ``<pre>`` is
additionally wrapped inside a ``<table>`` which has one row and two
cells: one containing the line numbers and one containing the code.
Example:

.. sourcecode:: html

    <div class="highlight" >
    <table><tr>
      <td class="linenos" title="click to toggle"
        onclick="with (this.firstChild.style)
                 { display = (display == '') ? 'none' : '' }">
        <pre>1
        2</pre>
      </td>
      <td class="code">
        <pre><span class="Ke">def </span><span class="NaFu">foo</span>(bar):
          <span class="Ke">pass</span>
        </pre>
      </td>
    </tr></table></div>

(whitespace added to improve clarity).

Wrapping can be disabled using the `nowrap` option.

A list of lines can be specified using the `hl_lines` option to make these
lines highlighted (as of Pygments 0.11).

With the `full` option, a complete HTML 4 document is output, including
the style definitions inside a ``<style>`` tag, or in a separate file if
the `cssfile` option is given.

When `tagsfile` is set to the path of a ctags index file, it is used to
generate hyperlinks from names to their definition.  You must enable
`anchorlines` and run ctags with the `-n` option for this to work.  The
`python-ctags` module from PyPI must be installed to use this feature;
otherwise a `RuntimeError` will be raised.

The `get_style_defs(arg='')` method of a `HtmlFormatter` returns a string
containing CSS rules for the CSS classes used by the formatter. The
argument `arg` can be used to specify additional CSS selectors that
are prepended to the classes. A call `fmter.get_style_defs('td .code')`
would result in the following CSS classes:

.. sourcecode:: css

    td .code .kw { font-weight: bold; color: #00FF00 }
    td .code .cm { color: #999999 }
    ...

If you have Pygments 0.6 or higher, you can also pass a list or tuple to the
`get_style_defs()` method to request multiple prefixes for the tokens:

.. sourcecode:: python

    formatter.get_style_defs(['div.syntax pre', 'pre.syntax'])

The output would then look like this:

.. sourcecode:: css

    div.syntax pre .kw,
    pre.syntax .kw { font-weight: bold; color: #00FF00 }
    div.syntax pre .cm,
    pre.syntax .cm { color: #999999 }
    ...

Additional options accepted:

`nowrap`
    If set to ``True``, don't wrap the tokens at all, not even inside a ``<pre>``
    tag. This disables most other options (default: ``False``).

`full`
    Tells the formatter to output a "full" document, i.e. a complete
    self-contained document (default: ``False``).

`title`
    If `full` is true, the title that should be used to caption the
    document (default: ``''``).

`style`
    The style to use, can be a string or a Style subclass (default:
    ``'default'``). This option has no effect if the `cssfile`
    and `noclobber_cssfile` option are given and the file specified in
    `cssfile` exists.

`noclasses`
    If set to true, token ``<span>`` tags will not use CSS classes, but
    inline styles. This is not recommended for larger pieces of code since
    it increases output size by quite a bit (default: ``False``).

`classprefix`
    Since the token types use relatively short class names, they may clash
    with some of your own class names. In this case you can use the
    `classprefix` option to give a string to prepend to all Pygments-generated
    CSS class names for token types.
    Note that this option also affects the output of `get_style_defs()`.

`cssclass`
    CSS class for the wrapping ``<div>`` tag (default: ``'highlight'``).
    If you set this option, the default selector for `get_style_defs()`
    will be this class.

    .. versionadded:: 0.9
       If you select the ``'table'`` line numbers, the wrapping table will
       have a CSS class of this string plus ``'table'``, the default is
       accordingly ``'highlighttable'``.

`cssstyles`
    Inline CSS styles for the wrapping ``<div>`` tag (default: ``''``).

`prestyles`
    Inline CSS styles for the ``<pre>`` tag (default: ``''``).

    .. versionadded:: 0.11

`cssfile`
    If the `full` option is true and this option is given, it must be the
    name of an external file. If the filename does not include an absolute
    path, the file's path will be assumed to be relative to the main output
    file's path, if the latter can be found. The stylesheet is then written
    to this file instead of the HTML file.

    .. versionadded:: 0.6

`noclobber_cssfile`
    If `cssfile` is given and the specified file exists, the css file will
    not be overwritten. This allows the use of the `full` option in
    combination with a user specified css file. Default is ``False``.

    .. versionadded:: 1.1

`linenos`
    If set to ``'table'``, output line numbers as a table with two cells,
    one containing the line numbers, the other the whole code.  This is
    copy-and-paste-friendly, but may cause alignment problems with some
    browsers or fonts.  If set to ``'inline'``, the line numbers will be
    integrated in the ``<pre>`` tag that contains the code (that setting
    is *new in Pygments 0.8*).

    For compatibility with Pygments 0.7 and earlier, every true value
    except ``'inline'`` means the same as ``'table'`` (in particular, that
    means also ``True``).

    The default value is ``False``, which means no line numbers at all.

    **Note:** with the default ("table") line number mechanism, the line
    numbers and code can have different line heights in Internet Explorer
    unless you give the enclosing ``<pre>`` tags an explicit ``line-height``
    CSS property (you get the default line spacing with ``line-height:
    125%``).

`hl_lines`
    Specify a list of lines to be highlighted.

    .. versionadded:: 0.11

`linenostart`
    The line number for the first line (default: ``1``).

`linenostep`
    If set to a number n > 1, only every nth line number is printed.

`linenospecial`
    If set to a number n > 0, every nth line number is given the CSS
    class ``"special"`` (default: ``0``).

`nobackground`
    If set to ``True``, the formatter won't output the background color
    for the wrapping element (this automatically defaults to ``False``
    when there is no wrapping element [eg: no argument for the
    `get_syntax_defs` method given]) (default: ``False``).

    .. versionadded:: 0.6

`lineseparator`
    This string is output between lines of code. It defaults to ``"\n"``,
    which is enough to break a line inside ``<pre>`` tags, but you can
    e.g. set it to ``"<br>"`` to get HTML line breaks.

    .. versionadded:: 0.7

`lineanchors`
    If set to a nonempty string, e.g. ``foo``, the formatter will wrap each
    output line in an anchor tag with a ``name`` of ``foo-linenumber``.
    This allows easy linking to certain lines.

    .. versionadded:: 0.9

`linespans`
    If set to a nonempty string, e.g. ``foo``, the formatter will wrap each
    output line in a span tag with an ``id`` of ``foo-linenumber``.
    This allows easy access to lines via javascript.

    .. versionadded:: 1.6

`anchorlinenos`
    If set to `True`, will wrap line numbers in <a> tags. Used in
    combination with `linenos` and `lineanchors`.

`tagsfile`
    If set to the path of a ctags file, wrap names in anchor tags that
    link to their definitions. `lineanchors` should be used, and the
    tags file should specify line numbers (see the `-n` option to ctags).

    .. versionadded:: 1.6

`tagurlformat`
    A string formatting pattern used to generate links to ctags definitions.
    Available variables are `%(path)s`, `%(fname)s` and `%(fext)s`.
    Defaults to an empty string, resulting in just `#prefix-number` links.

    .. versionadded:: 1.6


Differences of this params in pygmentize.cgi 
############################################

1. Some of params are non-sence for WEB-based formatter, so pygmentize.cgi
   don't support those. These are:

   * cssfile
   * noclobber_cssfile
   * tagsfile
   * tagurlformat

2. You can specify hl_lines with special format:

   .. code-block:: pycon

      >>> req = urllib2.Request(URL, data=json.dumps({
      ...     "command": "format",
      ...     "params": {
      ...         "code": code,
      ...         "lang": "pycon",
      ...         "hl_lines": "1-10, 20, 30-40, range(40, 50, 5)",
      ...     }}))

Controlling the behaviour of pygmentize.cgi using with HTTP header
------------------------------------------------------------------

Accept-Encoding
###############
Now, modern browsers such as Google Chrome will always request
with ``Accept-Encoding: gzip;deflate`` and you can't change this
in Ajax library, so you don't warry about it if you are in Ajax.

But, if you will call pygmentize.cgi via some programming languages
such as PHP, python, they don't add this header automatically.

pygmentize.cgi now accept only ``gzip``, ``deflate`` (and of course
``identity``).

Accept
######
You can choise ``Content-Type`` of the response either
``application/json`` or ``text/html``.

In "command": "format" case, ``application/json`` response makes
no sense (just ``string`` of json).

In "command": "lexers" case,

* ``application/json``: simple list encoded with json
* ``text/html``: html table

Accept-Charset
##############
When ``Accept`` is not ``application/json``, you can change charset of
html document (default: ``utf-8``). If ``Accept`` is ``application/json``, this is ignored
(always encode with ``utf-8``).
