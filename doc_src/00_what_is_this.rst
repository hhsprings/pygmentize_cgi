.. #
   # Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings>
   # All rights reserved.
   # 
   # Redistribution and use in source and binary forms, with or without
   # modification, are permitted provided that the following conditions
   # are met:
   # 
   # - Redistributions of source code must retain the above copyright
   #   notice, this list of conditions and the following disclaimer.
   # 
   # - Redistributions in binary form must reproduce the above copyright
   #   notice, this list of conditions and the following disclaimer in
   #   the documentation and/or other materials provided with the
   #   distribution.
   # 
   # - Neither the name of the hhsprings nor the names of its contributors
   #   may be used to endorse or promote products derived from this software
   #   without specific prior written permission.
   # 
   # THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   # "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
   # TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   # PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
   # CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   # EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   # PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   # PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   # LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   # NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   # SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   #

What is this?
*************

pygmentize.cgi is a CGI for HTML-based syntax highlighting by
`Pygments <http://pygments.org/>`_,
with almost all functionality of `Pygments <http://pygments.org/>`_
and for client's simple usage, for host's simple setup.

This is designed for the server with `Python <https://www.python.org/>`_ and `Pygments <http://pygments.org/>`_, for the client without both.

Note that it has **no caching mechanism**, because it is designed for
simply bundle to other product like WordPress's plugin, so,
maybe this is very slow and will use a lot of CPU, memory resource.
Don't use it on the site which has huge PV without your own
caching mechanism. The developpers of like WordPress plugin should
implement cache, for your user's experiences.
