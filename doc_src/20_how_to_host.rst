.. #
   # Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings>
   # All rights reserved.
   # 
   # Redistribution and use in source and binary forms, with or without
   # modification, are permitted provided that the following conditions
   # are met:
   # 
   # - Redistributions of source code must retain the above copyright
   #   notice, this list of conditions and the following disclaimer.
   # 
   # - Redistributions in binary form must reproduce the above copyright
   #   notice, this list of conditions and the following disclaimer in
   #   the documentation and/or other materials provided with the
   #   distribution.
   # 
   # - Neither the name of the hhsprings nor the names of its contributors
   #   may be used to endorse or promote products derived from this software
   #   without specific prior written permission.
   # 
   # THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   # "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
   # TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   # PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
   # CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   # EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   # PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   # PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   # LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   # NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   # SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   #

How to host your own Syntax Highliting Service using with pygmentize.cgi?
*************************************************************************

Requirement
-----------

1. `Python <https://www.python.org/>`_

  * maybe upper Python 2.7
  * or Python 3.x

2. The HTTP server that has capability of executing borne-shell script as CGI.

3. `Pygments <http://pygments.org/>`_

4. `distribution files <https://bitbucket.org/hhsprings/pygmentize_cgi>`_ of pygmentize.cgi

   Simply, you just need:

   * pygmentize.cgi
   * pygmentize.cgi.cfg
   * pygmentize_cgi.py
   * iana_character_sets.py
   * character-sets-1.csv

   Optionaly, if you need responce with document page(this page) when
   the client request to pygmentize.cgi with HTTP GET:

   * pygmentize_cgi.html
   * pygmentize_cgi_example.html
   * _static/

Setup
-----

If you are lucky
################
If you are lucky, just deploy distribution files of `pygmentize.cgi`
to proper location (commonly `/cgi-bin/`), and just add permission
for executing to `pygmentize.cgi`, that's the all.

Please see your server documents how to use CGI on your server,
and I don't explain like .htaccess, etc.


If the python on your server is not python
##########################################
Like a rental server, for the possible coexistence of multiple python
versions, python's name or path is sometimes abnormal, like
`/usr/opt/bin/python2.7`.

If so, please edit `pygmentize.cgi.cfg`:

.. code-block:: text

  python=/usr/opt/bin/python2.7
  PYTHONPATH=


If Pygments is installed on your server, but it's path is not normal
####################################################################
If `Pygments <http://pygments.org/>`_ is installed to different
location with normal, or you want to use specific version,
and so if `pygmentize.cgi` can't find it, edit `pygmentize.cgi.cfg`,
too.

.. code-block:: text

  python=/usr/opt/bin/python2.7
  PYTHONPATH=/home/youraccount/lib/python/site-packages


If Pygments is not installed on your server
###########################################
If you have access-rights system-widely, i.e. you have the root
account, just install it. Ordinally you can like this:

.. code-block:: console

  youraccount@yourserver: youraccount$ su
  password: ********
  root@yourserver: youraccount# pip install pygments

Or if you have no pip, you can install manually from source tree:

.. code-block:: console

  youraccount@yourserver: youraccount$ ls
  Pygments-2.0.2.tar.gz
  youraccount@yourserver: youraccount$ tar zxvf Pygments-2.0.2.tar.gz
    ...
  youraccount@yourserver: youraccount$ cd Pygments-2.0.2
  youraccount@yourserver: Pygments-2.0.2$ python setup.py build
    ...
  youraccount@yourserver: Pygments-2.0.2$ su
  password: ********
  root@yourserver: Pygments-2.0.2# python setup.py install
    ...

If you can't access for writing of system location like `/usr`,
you can deploy `Pygments <http://pygments.org/>`_ packages and modules
to your favor location, so edit `pygmentize.cgi.cfg`, too.

.. code-block:: text

  python=/usr/opt/bin/python2.7
  PYTHONPATH=/home/youraccount/lib/python/site-packages

BTW, if you have downloaded `Pygments source tree <https://bitbucket.org/birkenfeld/pygments-main/src>`_, and you want to upload to your server, you don't need to upload whole tree but just need to upload `pygments` subdir.
