.. #
   # Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings>
   # All rights reserved.
   # 
   # Redistribution and use in source and binary forms, with or without
   # modification, are permitted provided that the following conditions
   # are met:
   # 
   # - Redistributions of source code must retain the above copyright
   #   notice, this list of conditions and the following disclaimer.
   # 
   # - Redistributions in binary form must reproduce the above copyright
   #   notice, this list of conditions and the following disclaimer in
   #   the documentation and/or other materials provided with the
   #   distribution.
   # 
   # - Neither the name of the hhsprings nor the names of its contributors
   #   may be used to endorse or promote products derived from this software
   #   without specific prior written permission.
   # 
   # THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   # "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
   # TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   # PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
   # CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   # EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   # PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   # PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   # LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   # NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   # SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   #

How to highlight your code with pygmentize.cgi via HTTP?
********************************************************

Ajax
----
Example Using with Ajax `<pygmentize_cgi_example.html>`_ is available.

Generate Lexers Pulldown
########################
HTML:

.. code-block:: html

  <select id="lexer"/>

script:

.. code-block:: js

  jQuery(function($) {
    var syntaxService = "http://path/to/yourserver/cgi-bin/pygmentize.cgi";

    $(document).ready(function() {
      $.ajax({
         url: syntaxService,
         type: "POST", // MUST BE POST
         data: JSON.stringify({
           command: "lexers",
           params: {
             fields: ["name", "aliases"]
           }
         }),
         success: function(data) {
           res = "";
           for (i in data) {
             fullname = data[i][0];
             name = data[i][1][0];
             res += "<option value='" + name + "'";
             if (name == 'pycon') {
               res += " selected";
             }
             res += ">" + fullname + "</option>";
           }
           $('#lexer').html(res);
         },
         contentType: "application/json",
         headers: {'Accept': 'application/json'},
         dataType: "json",
      });
    });
  });

Generate Syntax Highliting Code
###############################

HTML:

.. code-block:: html

  <textarea id="source" rows="12" cols="120"></textarea>
  <div id="result"/>

script:

.. code-block:: js

  jQuery(function($) {
    var syntaxService = "http://path/to/yourserver/cgi-bin/pygmentize.cgi";

    function call_formatter(lang, code, fn)
    {
      $.ajax({
        url: syntaxService,
        type: "POST",  // MUST BE POST
        data: JSON.stringify({
          command: "format",
          params: {
            lang: lang,
            code: code,
          }
        }),
        contentType: "application/json",
        headers: {'Accept': 'text/html'},
        success: fn,
        dataType: "html",
      });
    }
    function highlight() {
      code = $('#source').val();
      call_formatter($('#lexer').val(), code, function(result) {
        $('#result').html(result);
      });
    }
    $(document).ready(function() {
      // ...
      $('#source').on('change', function() { highlight(); });
      $('#lexer').on('change', function() { highlight(); });
    });
  });

PHP
---

Prepare Common Utility
######################

.. code-block:: php

  <?php
  /*
   * call pygmentize.cgi as CGI.
   */
  function call_pygmentize_cgi_webservice($params, $outtype, &$error) {
  
      $curl = curl_init();
  
      // User Agent should be set against for WAF (Web Application Firewall)
      // or some.htaccess settings.
      $ua = '...';
      $verbose_stream = tmpfile(); // receiver stream for cURL verbose output
      curl_setopt_array($curl, array(
          CURLOPT_HEADER         => FALSE,
          CURLOPT_RETURNTRANSFER => TRUE,
          CURLOPT_VERBOSE        => TRUE,
          CURLOPT_STDERR         => $verbose_stream,
          CURLOPT_FAILONERROR    => TRUE, // fail on error code >= 400
          //CURLOPT_USERAGENT => $ua,  // recommended
          CURLOPT_REFERER => home_url('/'),
          CURLOPT_ENCODING => "gzip,deflate",  // if you want
      ));
      $header = array(
          "Accept-Charset: utf-8",
          "Content-Type: application/json",
      );
      if ($outtype == "json") {
          $header[] = "Accept: application/json";
      } else {
          $header[] = "Accept: text/html";
      }
      curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
  
      curl_setopt($curl, CURLOPT_URL, $cgipath);  // URL of pygmentize.cgi
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_POST, 1);  // MUST BE POST
  
      $post_data = json_encode($params);
  
      curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
      $output = curl_exec($curl);
  
      if ($output === FALSE) {
          // any code what you want
          $error = TRUE;
      }
      curl_close($curl);
  
      return $output;
  }

Generate Lexers Pulldown
########################

.. code-block:: php

  <?php
  /*
   * get lexers
   */
  function get_pygments_lexers() {
      $service_params = array(
          "command" => "lexers",
          "params" => array(
              "fields" => array("name", "aliases")
          )
      );
      $output = call_pygmentize_cgi_webservice(
          $service_params, "json", $error);
      $output = json_decode($output);
      if (!$error) {
          $result = "";
          foreach ($output as $row) {
              $fullname = $row[0];
              $name = $row[1][0];
              $result .= '<option value="';
              $result .= $name;
              $result .= '"';
              if ($name == 'php') {
                  $result .= ' selected';
              }
              $result .= ">";
              $result .= $fullname;
              $result .= "</option>";
          }
          return $result;
      }
      return "";
  }

  $lexers = get_pygments_lexers();
  ?>
  <select id="lang-pulldown"><?php echo $lexers ?></select>


Generate Syntax Highliting Code
###############################
.. code-block:: php

  <?php
  function entities_to_unicode($str, $flags) {
      $str = html_entity_decode($str, $flags, 'UTF-8');
      $str = preg_replace_callback(
          "/(&#[0-9]+;)/", function($m) {
              return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES");
          }, $str);
      return $str;
  }
  // ...
  $content = "<?php echo 'blah blah blah' ?>";
  // ...
      if (TRUE) { // if you want
          // mainly, the purpose of this code block is to
          // become friendly with visual editor.
          $flags = ENT_QUOTES | ENT_COMPAT | ENT_HTML401;
          $content = entities_to_unicode(
              preg_replace(
                  array('/&nbsp;/', '/&apos;/'),
                  array(' ', "'"),
                  $content), $flags);
      }
      /* ------------------------------ */
      $error = FALSE;
      $service_params = array(
          "command" => "format",
          "params" => array(
              'code' => $content,
              'lang' => $lang,
          )
      );
      $output = call_pygmentize_cgi_webservice(
          $service_params, "html", $error);

Python
------
Because pygments is written by Python,
so maybe this makes no sense for many pythonista, but it is very useful for trouble-shooting.

Enumerate Lexers
################
.. code-block:: pycon

  >>> URL = "http://path/to/yourserver/cgi-bin/pygmentize.cgi"
  >>> try:
  ...     import urllib2
  ...     from StringIO import StringIO as BytesIO
  ... except ImportError:  # Python 3.x
  ...     import urllib.request as urllib2
  ...     from io import BytesIO
  ...
  >>> import json
  >>> import sys
  >>> import zlib
  >>> import gzip
  >>> if sys.platform == "win32":
  ...     import os, msvcrt
  ...     msvcrt.setmode(sys.stdout.fileno(), os.O_BINARY)
  ...     msvcrt.setmode(sys.stderr.fileno(), os.O_BINARY)
  ...
  16384
  16384
  >>> req = urllib2.Request(URL, data=json.dumps({
  ...     "command": "lexers",
  ...     "params": {"fields": ["name", "aliases", "filenames", "mimetypes"]},
  ... }))
  >>> req.add_header("Accept", "application/json")
  >>> req.add_header("Accept-Encoding", "gzip")
  >>> req.add_header("Content-Type", "application/json")
  >>> handler = urllib2.HTTPHandler()
  >>> opener = urllib2.build_opener(handler)
  >>> f = opener.open(req)
  >>> print(f.headers)
  Date: Wed, 08 Jul 2015 10:05:52 GMT
  Server: Apache
  Content-Encoding: gzip
  Vary: Accept-Encoding,User-Agent
  Content-Length: 7106
  Cache-Control: max-age=31536000
  Expires: Thu, 07 Jul 2016 10:05:52 GMT
  Content-Type: application/json
  Connection: close
  
  >>> out = BytesIO()
  >>> out.write(f.read())
  >>> out.seek(0)
  >>> gzf = gzip.GzipFile(fileobj=out, mode='r')
  >>> d = gzf.read()
  >>> json.loads(d)[0]
  [u'ABAP', [u'abap'], [u'*.abap'], [u'text/x-abap']]
  >>> json.loads(d)[1]
  [u'ANTLR', [u'antlr'], [], []]
  >>> json.loads(d)[2]
  [u'ANTLR With ActionScript Target', [u'antlr-as', u'antlr-actionscript'], [u'*.G', u'*.g'], []]


Generate Syntax Highliting Code
###############################

.. code-block:: pycon

  >>> URL = "http://path/to/yourserver/cgi-bin/pygmentize.cgi"
  >>> try:
  ...     import urllib2
  ...     from StringIO import StringIO as BytesIO
  ... except ImportError:  # Python 3.x
  ...     import urllib.request as urllib2
  ...     from io import BytesIO
  ...
  >>> import json
  >>> import sys
  >>> import zlib
  >>> import gzip
  >>> if sys.platform == "win32":
  ...     import os, msvcrt
  ...     msvcrt.setmode(sys.stdout.fileno(), os.O_BINARY)
  ...     msvcrt.setmode(sys.stderr.fileno(), os.O_BINARY)
  ...
  16384
  16384
  >>>
  >>> code = """
  ... >>> import os
  ... >>> os.listdir(".")  # list current dir
  ... ['hoge.cpp', 'hoge.pyx', 'hoge.pxd', 'hoge.so']
  ... """
  >>> req = urllib2.Request(URL, data=json.dumps({
  ...     "command": "format",
  ...     "params": {
  ...         "code": code,
  ...         "lang": "pycon",
  ...         "linenos": "inline",
  ...     }}))
  >>> req.add_header("Accept", "text/html")
  >>> req.add_header("Accept-Encoding", "deflate")
  >>> req.add_header("Content-Type", "application/json")
  >>> handler = urllib2.HTTPHandler()
  >>> opener = urllib2.build_opener(handler)
  >>> f = opener.open(req)
  >>> print(f.headers)
  Date: Wed, 08 Jul 2015 10:40:02 GMT
  Server: Apache
  Content-Encoding: deflate
  Content-Length: 203
  Cache-Control: max-age=3600
  Expires: Wed, 08 Jul 2015 11:40:02 GMT
  Vary: User-Agent
  Content-Type: text/html; charset=iso-8859-1
  Connection: close
  
  >>> d = zlib.decompress(f.read())
  >>> d
  '<div class="highlight"><pre><span class="lineno">1 </span><span class="gp">&gt;&gt;&gt; </span><span class="kn">import</span> <span class="nn">os</span>\n<span class="lineno">2 </span><span class="gp
  ">&gt;&gt;&gt; </span><span class="n">os</span><span class="o">.</span><span class="n">listdir</span><span class="p">(</span><span class="s">&quot;.&quot;</span><span class="p">)</span>  <span class="
  c1"># list current dir</span>\n<span class="lineno">3 </span><span class="go">[&#39;hoge.cpp&#39;, &#39;hoge.pyx&#39;, &#39;hoge.pxd&#39;, &#39;hoge.so&#39;]</span>\n</pre></div>\n'
  >>> 
