# -*- coding: utf-8 -*-
#
# Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 
# - Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# 
# - Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
# 
# - Neither the name of the xwhhsprings nor the names of its contributors
#   may be used to endorse or promote products derived from this software
#   without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
import unittest
try:
    import urllib2
    from StringIO import StringIO as BytesIO
except ImportError:  # Python 3.x
    import urllib.request as urllib2
    from io import BytesIO

import json
import sys
import zlib
import gzip
if sys.platform == "win32":
    import os, msvcrt
    msvcrt.setmode(sys.stdout.fileno(), os.O_BINARY)
    msvcrt.setmode(sys.stderr.fileno(), os.O_BINARY)
try:
    # python 3
    import urllib.parse
    def unquote(s):
        return urllib.parse.unquote(s)
    def quote(s):
        return urllib.parse.quote(s)
except ImportError:
    import urllib
    def unquote(s):
        return urllib.unquote(s)
    def quote(s):
        return urllib.quote(s)


SERVICE_URL = "http://localhost:8000/cgi-bin/pygmentize.cgi";

_POSTDATA_00 = json.dumps(
    {
        'command': 'format',
        'params': {
            'code': u'# \u00b6 Paragraph sign',
            'lang': 'pycon',
            'full': True,
            },
        },
    ).encode("iso-8859-1")
_POSTDATA_01 = json.dumps(
    {
        'command': 'format',
        'params': {
            'code': u'>>> import os  # \u65e5\u672c\u8a9e\u30b3\u30e1\u30f3\u30c8',
            'lang': 'pycon',
            'full': True,
            },
        },
    ).encode("us-ascii")
_POSTDATA_02 = json.dumps(
    {
        'command': 'lexers',
        'params': {
            'fields': ["name", "aliases", "filenames", "mimetypes"],
            },
        },
    ).encode("us-ascii")


def _dump_as_getparam(d):
    def _buildkey(path):
        return quote("".join([
                ("[%s]" % p) if i > 0 else p
                for i, p in enumerate(path)]))
    def _enc(s):
        if isinstance(s, (int, bool,)):
            return str(s)
        return s.encode("utf-8")
    def _inner(result, d, path):
        if isinstance(d, (dict,)):
            for k in d:
                if isinstance(d[k], (dict, list,)):
                    _inner(result, d[k], path + [k])
                else:  # leaf
                    result.append("%s=%s" % (
                            _buildkey(path + [k]),
                            quote(_enc(d[k]))))
        else:
            for k in range(len(d)):
                if isinstance(d[k], (dict, list,)):
                    _inner(result, d[k], path + [""])
                else:  # leaf
                    result.append("%s=%s" % (
                            _buildkey(path + [""]),
                            quote(_enc(d[k]))))

    res = []
    _inner(res, d, [])
    return "&".join(res)


class TestAsCGI(unittest.TestCase):

    def setUp(self):
        pass

    def _call(self, postdata, headers, send_params_json=True):
        if postdata and not send_params_json:
            postdata = _dump_as_getparam(json.loads(postdata.decode("utf-8"))).encode("utf-8")
        req = urllib2.Request(SERVICE_URL, data=postdata)
        for k in headers:
            req.add_header(k, headers[k])
        #print(req.headers)
        handler = urllib2.HTTPHandler(debuglevel=0)
        opener = urllib2.build_opener(handler)

        f = opener.open(req)
        #print(f.headers)
        #print(f.headers.get("Status"))
        return f

    # --------------------------------------------------------
    def testHilights_success_00(self):
        #print("testHilights_success_00")
        f = self._call(_POSTDATA_00, {})
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        d = f.read().decode("iso-8859-1")

    def testHilights_success_00_02(self):
        #print("testHilights_success_00_02")
        f = self._call(_POSTDATA_00, {}, False)
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        d = f.read().decode("iso-8859-1")

    def testHilights_success_01(self):
        #print("testHilights_success_01")
        f = self._call(_POSTDATA_01, {
                "Accept-Charset": "utf-8"})
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        f.read().decode("utf-8")

    def testHilights_success_01_02(self):
        #print("testHilights_success_01_02")
        f = self._call(_POSTDATA_01, {
                "Accept-Charset": "utf-8"}, False)
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        f.read().decode("utf-8")

    def testHilights_success_02(self):
        #print("testHilights_success_02")
        f = self._call(_POSTDATA_01, {
                "Accept-Charset": "*"})
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        f.read().decode("utf-8")

    def testHilights_success_02_02(self):
        #print("testHilights_success_02_02")
        f = self._call(_POSTDATA_01, {
                "Accept-Charset": "*"}, False)
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        f.read().decode("utf-8")

    def testHilights_success_03(self):
        #print("testHilights_success_03")
        f = self._call(_POSTDATA_01, {
                "Accept-Charset": "MS_Kanji",
                "Accept": "text/html"})
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        f.read().decode("cp932")

    def testHilights_success_03_02(self):
        #print("testHilights_success_03_02")
        f = self._call(_POSTDATA_01, {
                "Accept-Charset": "MS_Kanji",
                "Accept": "text/html"}, False)
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        f.read().decode("cp932")

    def testHilights_success_04(self):
        #print("testHilights_success_04")
        f = self._call(_POSTDATA_01, {
                "Accept-Charset": "Hiso-2015-2-P,shift-jis",
                "Accept": "text/html"})
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        f.read().decode("cp932")

    def testHilights_success_04_02(self):
        #print("testHilights_success_04_02")
        f = self._call(_POSTDATA_01, {
                "Accept-Charset": "Hiso-2015-2-P,shift-jis",
                "Accept": "text/html"}, False)
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        f.read().decode("cp932")

    def testHilights_success_05(self):
        #print("testHilights_success_05")
        f = self._call(_POSTDATA_01, {
                "Accept-Charset": "shift-jis;q=0.9,iso-2022-jp",
                "Accept": "text/html"})
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        f.read().decode("iso-2022-jp")

    def testHilights_success_05_02(self):
        #print("testHilights_success_05_02")
        f = self._call(_POSTDATA_01, {
                "Accept-Charset": "shift-jis;q=0.9,iso-2022-jp",
                "Accept": "text/html"}, False)
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        f.read().decode("iso-2022-jp")

    def testHilights_success_06(self):
        #print("testHilights_success_06")
        f = self._call(_POSTDATA_01, {
                "Accept-Encoding": "gzip;q=1,deflate;q=0.9",
                "Accept-Charset": "utf-8",
                "Accept": "text/html"})
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        gzip.GzipFile(fileobj=f, mode='r')

    def testHilights_success_06_02(self):
        #print("testHilights_success_06_02")
        f = self._call(_POSTDATA_01, {
                "Accept-Encoding": "gzip;q=1,deflate;q=0.9",
                "Accept-Charset": "utf-8",
                "Accept": "text/html"}, False)
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        gzip.GzipFile(fileobj=f, mode='r')

    def testHilights_success_07(self):
        #print("testHilights_success_07")
        f = self._call(_POSTDATA_01, {
                "Accept-Encoding": "*",
                "Accept-Charset": "*",
                "Accept": "text/html"})
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        out = BytesIO()
        out.write(f.read())
        out.seek(0)

        gzf = gzip.GzipFile(fileobj=out, mode='r')
        d = gzf.read()
        d.decode("utf-8")

    def testHilights_success_07_02(self):
        #print("testHilights_success_07_02")
        f = self._call(_POSTDATA_01, {
                "Accept-Encoding": "*",
                "Accept-Charset": "*",
                "Accept": "text/html"}, False)
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        out = BytesIO()
        out.write(f.read())
        out.seek(0)

        gzf = gzip.GzipFile(fileobj=out, mode='r')
        d = gzf.read()
        d.decode("utf-8")

    def testHilights_success_08(self):
        #print("testHilights_success_08")
        f = self._call(_POSTDATA_01, {
                "Accept-Encoding": "gzip;q=0.8,deflate;q=0.9",
                "Accept-Charset": "utf-8",
                "Accept": "text/html"})
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        zlib.decompress(f.read()).decode("utf-8")

    def testHilights_success_08_02(self):
        #print("testHilights_success_08_02")
        f = self._call(_POSTDATA_01, {
                "Accept-Encoding": "gzip;q=0.8,deflate;q=0.9",
                "Accept-Charset": "utf-8",
                "Accept": "text/html"}, False)
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        zlib.decompress(f.read())

    def testHilights_success_09(self):
        #print("testHilights_success_09")
        f = self._call(_POSTDATA_01, {
                "Accept-Encoding": "gzip;q=1,deflate;q=0.9",
                "Accept-Charset": "iso-2022-jp",
                "Accept": "text/html"})
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        out = BytesIO()
        out.write(f.read())
        out.seek(0)

        gzf = gzip.GzipFile(fileobj=out, mode='r')
        d = gzf.read()
        d.decode("iso-2022-jp")

    def testHilights_success_09_02(self):
        #print("testHilights_success_09_02")
        f = self._call(_POSTDATA_01, {
                "Accept-Encoding": "gzip;q=1,deflate;q=0.9",
                "Accept-Charset": "iso-2022-jp",
                "Accept": "text/html"}, False)
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        out = BytesIO()
        out.write(f.read())
        out.seek(0)

        gzf = gzip.GzipFile(fileobj=out, mode='r')
        d = gzf.read()
        d.decode("iso-2022-jp")

    def testHilights_failure_01(self):
        #print("testHilights_failure_01")
        try:
            f = self._call(_POSTDATA_01, {
                    "Accept-Charset": "Hiso-2015-2-P",
                    "Accept": "text/html"})
        except urllib2.HTTPError as e:
            self.assertEqual(406, e.getcode())

    def testHilights_failure_01_02(self):
        #print("testHilights_failure_01_02")
        try:
            f = self._call(_POSTDATA_01, {
                    "Accept-Charset": "Hiso-2015-2-P",
                    "Accept": "text/html"}, False)
        except urllib2.HTTPError as e:
            self.assertEqual(406, e.getcode())

    def testHilights_failure_02(self):
        #print("testHilights_failure_02")
        try:
            f = self._call(_POSTDATA_01, {
                    "Accept-Encoding": "gzip;q=0,deflate;q=0,identity;q=0",
                    "Accept": "text/html"})
        except urllib2.HTTPError as e:
            self.assertEqual(406, e.getcode())

    def testHilights_failure_02_02(self):
        #print("testHilights_failure_02_02")
        try:
            f = self._call(_POSTDATA_01, {
                    "Accept-Encoding": "gzip;q=0,deflate;q=0,identity;q=0",
                    "Accept": "text/html"}, False)
        except urllib2.HTTPError as e:
            self.assertEqual(406, e.getcode())

    # --------------------------------------------------------
    def testLexers_success_01(self):
        #print("testLexers_success_01")
        f = self._call(_POSTDATA_02, {"Accept": "application/json"})
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        json.loads(f.read().decode("utf-8"))

    def testLexers_success_01_02(self):
        #print("testLexers_success_01_02")
        f = self._call(_POSTDATA_02, {"Accept": "application/json"}, False)
        self.assertEqual("200", f.headers.get("Status")[:len("200")])
        json.loads(f.read().decode("utf-8"))

    # --------------------------------------------------------
    def test_failure_01(self):
        #print("test_failure_01")
        f = self._call(
            json.dumps({"command": "uso800", "params": {}}).encode("utf-8"),
            {"Accept": "text/html"})
        self.assertEqual("400", f.headers.get("Status")[:len("200")])

    def test_failure_01_02(self):
        #print("test_failure_01_02")
        f = self._call(
            json.dumps({"command": "uso800", "params": {}}).encode("utf-8"),
            {"Accept": "text/html"}, False)
        self.assertEqual("400", f.headers.get("Status")[:len("200")])


def _testserver():
    import os
    os.chdir("..")
    import testcgihttpserver
    testcgihttpserver.test()


if __name__ == '__main__':
    from multiprocessing import Process

    p = Process(target=_testserver)
    p.start()
    import time
    time.sleep(5)
    try:
        sys.argv = [
            sys.argv[0],
            #"TestAsCGI.testHilights_success_02",
            #"TestAsCGI.testHilights_success_06",
            #"TestAsCGI.testLexers_success_01",
            #"TestAsCGI.testLexers_success_01",
            #"TestAsCGI.testHilights_success_07_02",
            #"TestAsCGI.test_failure_01",
            #"TestAsCGI.test_failure_01_02",
            #"TestAsCGI.test_failure_02",
            ]
        unittest.main()
    finally:
        p.terminate()
